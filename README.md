<img width="200" src="https://gitlab.com/kAoi97/brand/raw/master/Imagotype/Bitmap/kAoi97%20horisontal%20Imagotype.png" /><hr>

# WebMiner (Old Version)

`This repository is deprecated, please visit the new version ` [WebMiner](https://gitlab.com/kAoi97/webminer)

Below you will find useful information when you want to implement your own version of this web miner on your site. Remember that this is a simple [Webchain](https://webchain.network/) miner built to be mined via websites. Based on the script provided by CoinIMP for the Webchain websites mining.

## Changelog

If you have cloned this repository before, do not forget to check the [changelog](CHANGELOG.md) to make sure you are working with the _latest changes uploaded_ to the repository.

## Support

If you have any doubts or suggestions when using the elements provided in this repository, do not hesitate to contact me through the different means and networks available below, I will be attentive to answer any questions in the shortest possible time:

[www.kaoi97.net](https://www.kaoi97.net)  

[leonardo@kaoi97.net](mailto:leonardo@kaoi97.net)  

[(+57)314 4818811](https://t.me/kAoi97)  


## License

<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">
<img alt="Creative Commons Licence" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a>
<br />
<span xmlns:dct="http://purl.org/dc/terms/" property="dct:title">kAoi97 WebMiner</span> by <a xmlns:cc="http://creativecommons.org/ns#" href="https://www.kaoi97.net/leoElBaku" property="cc:attributionName" rel="cc:attributionURL">Leonardo Alvarado Guio</a>
is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>.<br />
Permissions beyond the scope of this license may be available at <a xmlns:cc="http://creativecommons.org/ns#" href="https://www.kaoi97.net/terms-and-conditions" rel="cc:morePermissions">https://www.kaoi97.net/terms-and-conditions</a>.
